from PIL import Image, ImageTk

import tkinter
import cv2


class VideoWindow(object):
    """
    Class represents filters that may be applied to the webcam video.
    """

    def __init__(self, width):
        """
        Initialization.
        :param width: Width of the webcam frame
        """
        self.capture = cv2.VideoCapture(0)
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, int(width / self.capture.get(cv2.CAP_PROP_FRAME_WIDTH) *
                                                        self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        self.__original_filter_action()

        # Filters' values.
        self.box_kernel_size = 10
        self.gaussian_sigma = 10
        self.median_kernel_size = 21
        self.bilateral_sigma_color = 50
        self.bilateral_distance_space = 12
        self.canny_threshold1 = 100
        self.canny_threshold2 = 200

        # Interface creating.
        self.root = tkinter.Tk()
        self.root.title("VideoFiltersCV")
        self.root.resizable(False, False)
        self.root.attributes("-topmost", True)
        self.button_frame = tkinter.Frame(self.root)
        self.button_frame.pack(side=tkinter.TOP)

        self.button_original = tkinter.Button(self.root, text="Original", command=self.__original_filter_action)

        self.button_binary = tkinter.Button(self.root, text="Binarization", command=self.__binary_filter_action)

        self.entry_box_kernel_size = tkinter.Entry(self.root, width=5)
        self.entry_box_kernel_size.insert(0, self.box_kernel_size)
        self.button_box = tkinter.Button(self.root, text="Box filter", command=self.__box_filter_action)

        self.entry_gaussian_sigma = tkinter.Entry(self.root, width=5)
        self.entry_gaussian_sigma.insert(0, self.gaussian_sigma)
        self.button_gaussian = tkinter.Button(self.root, text="Gaussian filter", command=self.__gaussian_filter_action)

        self.entry_median_kernel_size = tkinter.Entry(self.root, width=5)
        self.entry_median_kernel_size.insert(0, self.median_kernel_size)
        self.button_median = tkinter.Button(self.root, text="Median filter", command=self.__median_filter_action)

        self.entry_bilateral_sigma_color = tkinter.Entry(self.root, width=5)
        self.entry_bilateral_sigma_color.insert(0, self.bilateral_sigma_color)
        self.entry_bilateral_distance_space = tkinter.Entry(self.root, width=5)
        self.entry_bilateral_distance_space.insert(0, self.bilateral_distance_space)
        self.button_bilateral = tkinter.Button(self.root, text="Bilateral filter",
                                               command=self.__bilateral_filter_action)

        self.entry_canny_threshold1 = tkinter.Entry(self.root, width=5)
        self.entry_canny_threshold1.insert(0, self.canny_threshold1)
        self.entry_canny_threshold2 = tkinter.Entry(self.root, width=5)
        self.entry_canny_threshold2.insert(0, self.canny_threshold2)
        self.button_canny = tkinter.Button(self.root, text="Canny detection",
                                           command=self.__canny_edge_detector_action)

        # Packing.
        self.button_original.pack(in_=self.button_frame, side=tkinter.LEFT)

        self.button_binary.pack(in_=self.button_frame, side=tkinter.LEFT)

        self.entry_box_kernel_size.pack(in_=self.button_frame, side=tkinter.LEFT)
        self.button_box.pack(in_=self.button_frame, side=tkinter.LEFT)

        self.entry_gaussian_sigma.pack(in_=self.button_frame, side=tkinter.LEFT)
        self.button_gaussian.pack(in_=self.button_frame, side=tkinter.LEFT)

        self.entry_median_kernel_size.pack(in_=self.button_frame, side=tkinter.LEFT)
        self.button_median.pack(in_=self.button_frame, side=tkinter.LEFT)

        self.entry_bilateral_sigma_color.pack(in_=self.button_frame, side=tkinter.LEFT)
        self.entry_bilateral_distance_space.pack(in_=self.button_frame, side=tkinter.LEFT)
        self.button_bilateral.pack(in_=self.button_frame, side=tkinter.LEFT)

        self.entry_canny_threshold1.pack(in_=self.button_frame, side=tkinter.LEFT)
        self.entry_canny_threshold2.pack(in_=self.button_frame, side=tkinter.LEFT)
        self.button_canny.pack(in_=self.button_frame, side=tkinter.LEFT)

        self.video_panel = tkinter.Label(self.root)
        self.video_panel.pack(side=tkinter.BOTTOM, fill=tkinter.BOTH, expand=True)

        self.__play_video()
        self.__centerWindow(self.root)
        self.root.mainloop()
        self.capture.release()

    def __play_video(self):
        """
        Play video frame by frame.
        """
        is_video, frame = self.capture.read()
        if is_video:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = self.filter(frame)
            frame = Image.fromarray(frame)
            image_tk = ImageTk.PhotoImage(image=frame)
            self.video_panel.image_tk = image_tk
            self.video_panel.configure(image=image_tk)
            self.video_panel.after(20, self.__play_video)

    def __centerWindow(self, window):
        window.update_idletasks()
        width = window.winfo_width()
        height = window.winfo_height()
        x_coordinate = int(window.winfo_screenwidth() / 2 - width / 2)
        y_coordinate = int(window.winfo_screenheight() / 2 - height / 1.6)
        window.geometry("{0}x{1}+{2}+{3}".format(width, height, x_coordinate, y_coordinate))

    def __original_filter_action(self):
        self.filter = lambda frame: frame

    def __binary_filter_action(self):
        self.filter = lambda frame: cv2.cvtColor(cv2.threshold(cv2.cvtColor(
            frame, cv2.COLOR_BGR2GRAY), 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1], cv2.COLOR_GRAY2BGR)

    def __box_filter_action(self):
        self.box_kernel_size = int(self.entry_box_kernel_size.get())
        if self.box_kernel_size % 2 == 0:
            self.box_kernel_size += 1
        self.filter = lambda frame: cv2.boxFilter(frame, -1, (self.box_kernel_size, self.box_kernel_size))

    def __gaussian_filter_action(self):
        self.gaussian_sigma = float(self.entry_gaussian_sigma.get())
        self.filter = lambda frame: cv2.GaussianBlur(frame, (0, 0), self.gaussian_sigma)

    def __median_filter_action(self):
        self.median_kernel_size = int(self.entry_median_kernel_size.get())
        if self.median_kernel_size % 2 == 0:
            self.median_kernel_size += 1
        self.filter = lambda frame: cv2.medianBlur(frame, self.median_kernel_size)

    def __bilateral_filter_action(self):
        self.bilateral_sigma_color = float(self.entry_bilateral_sigma_color.get())
        self.bilateral_distance_space = int(self.entry_bilateral_distance_space.get())
        self.filter = lambda frame: cv2.bilateralFilter(frame, self.bilateral_distance_space,
                                                        self.bilateral_sigma_color, 50)

    def __canny_edge_detector_action(self):
        self.canny_threshold1 = int(self.entry_canny_threshold1.get())
        self.canny_threshold2 = int(self.entry_canny_threshold2.get())
        self.filter = lambda frame: cv2.Canny(frame, self.canny_threshold1, self.canny_threshold2)
