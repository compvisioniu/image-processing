# VideoFiltersCV - image processing filters applied to webcam video

##### It has simple GUI based on tkinter, thus it is easy to figure out how it works.

You can apply Binarization, Box filter, Gaussian filter, Median filter, Bilateral filter, Canny edge detection filter.